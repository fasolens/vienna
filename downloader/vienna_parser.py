import json
import os
from typing import List


def get_list_of_files() -> List:
    dir_name = 'raw_data'
    file_list = os.listdir(dir_name)
    file_list = [os.path.join(os.getcwd(), dir_name, file) for file in file_list]
    file_list.sort()
    return file_list


def load_files(file_list: List, fund_data: dict) -> None:
    for file in file_list:
        path = os.path.join(os.getcwd(), file)
        with open(path, 'r') as f:
            parse_json(fund_data, json.load(f))


def parse_json(fund_data: dict, json_data: json) -> None:
    for key, value in json_data.items():
        if key not in fund_data:
            fund_data[key] = {'Currency': value['Currency'],
                              'Price': {}
                              }
        fund_data[key]['Price'][value['Price_date']] = value['Unit_price']


def parse(fund_data: dict) -> None:
    for key in fund_data.keys():
        price_date_list = list(fund_data[key]['Price'].keys())
        price_date_list.sort()
        fund_data[key]['First_date'] = price_date_list[0]
        fund_data[key]['Last_date'] = price_date_list[-1]


def create_history(fund_data: dict) -> None:
    load_files(get_list_of_files(), fund_data)
    parse(fund_data)
    with open('funds.json', 'w', newline='') as f:
        json.dump(fund_data, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    fund_data = dict()
    create_history(fund_data)
