import json
from urllib import request


def create_download_url(year: int, month: int, day: int) -> str:
    if 2000 > year > 2100:
        assert False, "Wrong year given"
    if 1 > month > 12:
        assert False, "Wrong month given"
    if 1 > day > 31:
        assert False, "Wrong day given"

    if month < 10:
        month = "0" + str(month)
    if day < 10:
        day = "0" + str(day)

    # noinspection PyPep8
    download_url = "http://www.viennalife.pl/centrum-inwestycyjne/notowania/eksportuj/?export&date={0}-{1}-{2}&calcValues=".format(
        str(year), str(month), str(day)
    )
    return download_url


def dump_vienna_to_json(year: int, month: int, day: int) -> None:
    response = request.urlopen(create_download_url(year, month, day))
    csv_data = response.read()
    csv_data = csv_data.decode('cp1250')
    csv_data = csv_data.replace('"', '').replace(',', '.').replace(';', ',')
    fieldnames = ("Code", "Price_date", "Unit_price", "Currency", "Daily_change", "One_month_change",
                  "Three_month_change", "Six_month_change", "One_year_change", "Three_year_change", "Five_year_change",
                  "From_beginning")
    csv_data = csv_data.split('\n')

    # Removing header
    csv_data = csv_data[1:]

    vienna_dict = {}
    for row in csv_data:
        row_split = row.split(',')
        if len(row_split) == 12:
            vienna_dict[row_split[0]] = {fieldnames[1]: row_split[1],
                                         fieldnames[2]: row_split[2],
                                         fieldnames[3]: row_split[3],
                                         fieldnames[4]: row_split[4],
                                         fieldnames[5]: row_split[5],
                                         fieldnames[6]: row_split[6],
                                         fieldnames[7]: row_split[7],
                                         fieldnames[8]: row_split[8],
                                         fieldnames[9]: row_split[9],
                                         fieldnames[10]: row_split[10],
                                         fieldnames[11]: row_split[11], }
    if 2000 > year > 2100:
        assert False, "Wrong year given"
    if 1 > month > 12:
        assert False, "Wrong month given"
    if 1 > day > 31:
        assert False, "Wrong day given"

    if month < 10:
        month = "0" + str(month)
    if day < 10:
        day = "0" + str(day)
    with open('data_{0}-{1}-{2}.json'.format(year, month, day), 'w', newline='') as f:
        json.dump(vienna_dict, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    for year in range(2017, 2016, -1):
        for month in range(2, 0, -1):
            for day in range(31, 0, -1):
                dump_vienna_to_json(year, month, day)
